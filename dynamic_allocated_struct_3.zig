const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;


const Token = struct {
  token_type: ?[]u8,
  content: ?[]u8,
  allocator: Allocator,

  pub fn init(allocator: Allocator) *Token {
    var tmp = allocator.create(Token) catch unreachable;  
    tmp.* = Token{.token_type = null, .content = null, .allocator = allocator,};
    return tmp;
  }

  pub fn setType(self: *Token, x: []const u8) void {
    if(self.token_type != null) self.allocator.free(self.token_type.?);
    self.token_type = self.allocator.alloc(u8, x.len) catch unreachable; 
    std.mem.copy(u8, self.token_type.?, x);
  }

  pub fn setContent(self: *Token, x: []const u8) void {
    if(self.content != null) self.allocator.free(self.content.?);
    self.content = self.allocator.alloc(u8, x.len) catch unreachable; 
    std.mem.copy(u8, self.content.?, x);
  }

  pub fn deinit(self: *Token) void {
    if(self.token_type != null) self.allocator.free(self.token_type.?);
    if(self.content != null) self.allocator.free(self.content.?);
    self.allocator.destroy(self);
  }
};


pub fn main() !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();
  defer _ = gpa.deinit();
  


  var lst_tokens: []*Token = allocator.alloc(*Token, 1) catch unreachable;
  defer allocator.free(lst_tokens);

  lst_tokens[0]=Token.init(allocator);
  lst_tokens[0].*.setType("HEADER");
  lst_tokens[0].*.setContent("header");


  var lst2: []*Token = allocator.alloc(*Token, lst_tokens.len+1) catch unreachable;
  for(lst_tokens)|_, i|{
    lst2[i]=lst_tokens[i]; 
  }
  allocator.free(lst_tokens);
  lst_tokens = lst2;

  lst_tokens[1]=Token.init(allocator);
  lst_tokens[1].*.setType("HEADER2");
  lst_tokens[1].*.setContent("header2");

  //print everything
  for(lst_tokens)|token|{
    print("{s}||{s}\n", .{token.*.token_type.?, token.*.content.?});
  }

  for(lst_tokens)|token|{
    token.deinit();
  }
}
