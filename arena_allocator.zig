//arena allocator demo

const std = @import("std");
const print = std.debug.print;


pub fn main() !void {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();

  const allocator = arena.allocator();

  var string1 : []u8 = try allocator.alloc(u8, 3);
  std.mem.copy(u8, string1, "foo");
  print("{s}\n", .{string1});


  var string2 : []u8 = try allocator.alloc(u8, 3);
  std.mem.copy(u8, string2, "bar");
  print("{s}\n", .{string2});

  //no need to free either string1 or string2. Everything freed when arena.deinit()
  //see https://ziglang.org/documentation/master/#toc-Choosing-an-Allocator
}
