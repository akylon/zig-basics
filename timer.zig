// see Timer on std
// is this a non-blocking timer ??
// https://ziglang.org/documentation/master/std/src/std/time.zig.html#L278


const std = @import("std");
const print = std.debug.print;
const Timer = std.time.Timer;

const ns_per_us = 1000;
const ns_per_ms = 1000 * ns_per_us;


const SEC_IN_NANO = 1000000000;

pub fn main() !void {
    var timer = try Timer.start();
    // const time_0 = timer.lap();  //returns time then resets timer.
    var time_now = timer.read(); // nanoseconds
    while(time_now < 10*SEC_IN_NANO):(time_now = timer.read() ) {
    }
}
