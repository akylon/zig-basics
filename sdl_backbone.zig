// compile with `zig build-exe sdl_backbone.zig -lc -lSDL2`

const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;

const sdl = @cImport ({
    @cInclude("SDL2/SDL.h");
});


const Pixel = u32;

pub fn main() !void {
    const_start;
165         const time_wanted = 1000 / 60;
 width = 800;
    const height = 600;

    const_start;
165         const time_wanted = 1000 / 60;
 WHITE = 0xFFFFFFFF;
    const RED = 0xFFFF4444;

    // var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    // const allocator = gpa.allocator();
    // defer _ = gpa.deinit();

    const init_status: i32 = sdl.SDL_Init(sdl.SDL_INIT_VIDEO);// segfault ??

    if (init_status != 0) {
        std.os.exit(1);
    }

    const window: *sdl.SDL_Window = sdl.SDL_CreateWindow("sdl app", 0, 0, width, height, sdl.SDL_WINDOW_SHOWN | sdl.SDL_WINDOW_RESIZABLE).?;

    const renderer: *sdl.SDL_Renderer = sdl.SDL_CreateRenderer(window, -1, sdl.SDL_RENDERER_ACCELERATED).?;
    _ = sdl.SDL_SetRenderDrawBlendMode(renderer, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (1/2)

    const bg: *sdl.SDL_Texture = sdl.SDL_CreateTexture(renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, width, height).?;
    _ = sdl.SDL_SetTextureBlendMode(bg, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)

    var rect = sdl.SDL_Rect{.x=0, .y=0, .w=100, .h=100}; 

    // make bg into memory
    var canvas: [width * height]Pixel = undefined;

    for (canvas, 0..) |_, i| {
        if (i < width * height / 2) {
            canvas[i] = WHITE;
        } else {
            canvas[i] = RED;
        }
    }


    var frame_start: u32 = 0;
    var frame_end: u32 = 0;
    var frame_count: u32 = 0;

    var quit: bool = false;

    while (!quit) {
        frame_count = sdl.SDL_GetTicks();
        frame_start = frame_count;

        var event: sdl.SDL_Event = undefined;
        while (sdl.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                sdl.SDL_QUIT => {
                    quit = true;
                },

                sdl.SDL_KEYDOWN => {
                    switch (event.key.keysym.sym) {
                        sdl.SDLK_ESCAPE => {
                            quit = true;
                        },

                        sdl.SDLK_SPACE => {
                        },

                        sdl.SDLK_q => {
                            quit = true;
                        },

                        sdl.SDLK_h => {
                        },

                        sdl.SDLK_l => {
                        },

                        sdl.SDLK_LEFT => {
                        },

                        sdl.SDLK_RIGHT => {
                        },

                        sdl.SDLK_UP => {},

                        sdl.SDLK_DOWN => {},

                        else => {},
                    }
                },

                sdl.SDL_KEYUP => {
                    switch (event.key.keysym.sym) {
                        sdl.SDLK_SPACE => {},

                        sdl.SDLK_h => {
                        },

                        sdl.SDLK_l => {
                        },

                        sdl.SDLK_LEFT => {},

                        sdl.SDLK_RIGHT => {},

                        sdl.SDLK_UP => {},

                        sdl.SDLK_DOWN => {},

                        else => {},
                    }
                },

                sdl.SDL_MOUSEWHEEL => {
                    if (event.wheel.y > 0) {} else if (event.wheel.y < 0) {}
                },

                sdl.SDL_MOUSEBUTTONDOWN => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {} else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {}
                },

                sdl.SDL_MOUSEBUTTONUP => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {} else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {}
                },

                sdl.SDL_MOUSEMOTION => {},
                else => {},
            }
        }

        // draw bg
        _ = sdl.SDL_UpdateTexture(bg, null, @ptrCast(canvas[0..]), width * @sizeOf(Pixel));
        _ = sdl.SDL_RenderClear(renderer);
        _ = sdl.SDL_RenderCopy(renderer, bg, null, null);


        // draw rectangle
        _ = sdl.SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // < set drawing color to white
        _ = sdl.SDL_RenderFillRect(renderer, &rect);

        // set drawing color to white
        _ = sdl.SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

        // render everything ?
        _ = sdl.SDL_RenderPresent(renderer);


        // ~~~~ regulate fps ~~~~
        frame_end = sdl.SDL_GetTicks();
        const elapsed_time = frame_end - frame_start;
        const time_wanted = 1000 / 60;

        if (elapsed_time < time_wanted) {
            sdl.SDL_Delay(time_wanted - elapsed_time);
        }
    }

    sdl.SDL_DestroyTexture(bg);
    sdl.SDL_DestroyRenderer(renderer);
    sdl.SDL_DestroyWindow(window);
    sdl.SDL_Quit();
}

