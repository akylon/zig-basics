const std = @import("std");
const print = std.debug.print;


pub fn main() !void {
  print("{d}\n", .{std.time.milliTimestamp()});
}