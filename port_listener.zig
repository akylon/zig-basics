// example from : https://github.com/ziglang/zig/blob/master/lib/std/net/test.zig
// simple port listener

const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;
const net = std.net;

pub fn main() !void {

    const localhost = try net.Address.parseIp("127.0.0.1", 8080);
    var server = net.StreamServer.init(.{});
    defer server.deinit();

    try server.listen(localhost);

    var client = try server.accept();
    defer client.stream.close();
    
    const stream = client.stream.writer();
    try stream.print("hello from server\n", .{});

    // on linux terminal, do `nc localhost 8080`
    // will print "hello from server" on client

}
