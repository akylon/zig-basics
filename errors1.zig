const std = @import("std");
const print = std.debug.print;

const errors = error {
  zeroParam,
};

//fails if param != 0
pub fn zeroOrError(param: u16) !u16 {
  if(param==0){
    return 0;
  }
  else {
    return errors.zeroParam;
  }
}


//cannot fail
pub fn zeroNoError(param: u16) u16 {
  //if we have an error, don't fail but return 1
  return zeroOrError(param) catch return 1;
  //don't need try keyword
}


pub fn main() !void {
  print("{d}\n", .{try zeroOrError(0)});
  //print("{d}\n", .{try zeroOrError(1)}); //fails

  print("{d}\n", .{zeroNoError(0)});
  print("{d}\n", .{zeroNoError(1)});
}
