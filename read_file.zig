//open file and read line by line demo

const std = @import("std");
const print = std.debug.print;

const MAXSIZE = 1*1024*1024;

pub fn main() !void {

  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();
  defer _ = gpa.deinit();



  const file = try std.fs.cwd().openFile("foo.txt", .{});
  defer file.close();

  while(try file.reader().readUntilDelimiterOrEofAlloc(allocator, '\n', MAXSIZE)) |line| {
    print("{s}\n", .{line});
    allocator.free(line);
  }
}


