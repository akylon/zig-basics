//get user input demo


const std = @import("std");
const print = std.debug.print;
const stdin = std.io.getStdIn();


pub fn main() !void {

  //allocator & co
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer {
    const leaked = gpa.deinit();
    if(leaked) @panic("leaked");
  }


  //temporary buffer to store user input
  var buffer: [100]u8 = undefined;

  print("Enter input (100 chars max) : ", .{});

  _ = try stdin.reader().readUntilDelimiterOrEof(&buffer, '\n', );

  //determine input string length
  //value>126 considered as garbage
  var length : u8 = 0;
  while(buffer[length]<=126) : (length+=1) {}
  length-=1; //remove trailing \n


  var input : []u8 = try allocator.alloc(u8, length);
  defer allocator.free(input);

  for(input) |_, i|  
    input[i] = buffer[i];
  

  print("You entered : {s}\n", .{input});

}

