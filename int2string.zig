const std = @import("std");
const expect = std.testing.expect;


test "int2string" {
  var buffer: [20]u8 = undefined;
  const output = try std.fmt.bufPrint(buffer[0..], "{d}", .{70145});

  try expect(std.mem.eql(u8, output, "70145"));
}
