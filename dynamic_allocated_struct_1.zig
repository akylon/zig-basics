const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;


const Lex = struct {
  content: ?*const u16,
  allocator: Allocator,

  pub fn init(allocator: Allocator) *Lex {
    var tmp = allocator.create(Lex) catch unreachable;  
    tmp.* = Lex{.content = null, .allocator = allocator,};
    return tmp;
  }

  pub fn setStruct(self: *Lex, x: u16) void {
    self.content = &x;
  }

  pub fn deinit(self: *Lex) void {
    self.allocator.destroy(self);
  }
};




pub fn main() !void {

  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();
  defer  _ = gpa.deinit();

  var t = Lex.init(allocator);
  t.setStruct(72);
  print("{?}\n", .{t.content.?.*});
  defer t.deinit();
}
