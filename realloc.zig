const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;


pub fn main() !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer {
    const leaked = gpa.deinit();
    if(leaked) @panic("leaked");
  }
  
  var mystring = try allocator.alloc(u8, 3);
  defer allocator.free(mystring);
  
  std.mem.copy(u8, mystring, "FOO");
  print("{s}\n", .{mystring});
  print("{d}\n", .{mystring.len});
  
  mystring = try allocator.realloc(mystring, 6);
  std.mem.copy(u8, mystring, "FOOBAR");
  
  print("{s}\n", .{mystring});
  print("{d}\n", .{mystring.len});
  
}
