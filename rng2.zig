// inpired by : https://discord.com/channels/605571803288698900/1105463570856169482/1105463570856169482
const std = @import("std");
const print = std.debug.print;
const testing = std.testing;

/// returns a number between min and max (both included)
fn get_random_number(comptime T: type, min: T, max: T) !T {
    // FIXME : should we generate prng only once ?
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        try std.os.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    });

    const rand = prng.random();
    return rand.intRangeAtMost(T, min, max);
}

pub fn main() !void {
    var res = std.mem.zeroes([10]u32);
    var i: u32 = 0;
    while (i < 4000000) : (i += 1) {
        const nb = try get_random_number(u8, 0, 9);
        res[nb] += 1;
    }
    for (res, 0..) |nb, k| {
        print("{d} -> {d} \n", .{ k, nb });
    }
}

test "generate random number" {
    const nb = try get_random_number(u8, 0, 1);
    try testing.expect(nb == 0 or nb == 1);

    const nb2 = try get_random_number(u16, 256, 257);
    try testing.expect(nb2 == 256 or nb2 == 257);
}
