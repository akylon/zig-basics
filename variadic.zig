const std = @import("std");
const print = std.debug.print;


pub fn variadic(args: anytype) void {
  inline for(args)|arg|{
    print("{s}\n", .{arg});
  }
}


pub fn main() !void {
  variadic(.{"foo", "bar", "ba"});
}
