const std = @import("std");
const print = std.debug.print;

pub fn main () !void {

    const stdin = std.io.getStdIn().reader();
    var user_input_buffer: [10]u8 = undefined;

    print("hello world !\n", .{});

    const program_running: bool = true;

    while (program_running) {
        const user_input = (try stdin.readUntilDelimiterOrEof(&user_input_buffer, '\n')).?;
        print("{s}\n", .{user_input});
    }
}
