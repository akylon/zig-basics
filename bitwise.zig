const std = @import("std");
const assert = std.debug.assert;
const testing = std.testing;



test "bitwise" {

  const a: u8 = 0b00000011;
  const b: u8 = 0b00000110; 
  
  try testing.expectEqual(a&b, 0b00000010); // AND

  try testing.expectEqual(a|b, 0b00000111); // OR

  try testing.expectEqual(a^b, 0b00000101); // XOR

  try testing.expectEqual(~a, 0b11111100);  // NOT
  
  try testing.expectEqual(a<<1, 0b00000110);

  try testing.expectEqual(b<<2, 0b00011000);

  try testing.expectEqual(a>>1, 0b00000001);

  try testing.expectEqual(b>>2, 0b00000001);

  try testing.expectEqual(a<<7, 0b10000000);

  const zero: u8 = 0b00000001;
  try testing.expectEqual((zero<<|8), 255); //saturating bit shift left
  


  try testing.expectEqual((0b00000001<<8), 256); // careful ! This is quirky
                                                 // no overflow because comptime_int and not u8 !!
}


test "bitwise assign" {
    const c: u8 = 0b11;
    var d: u8 = 0b101;

    d &= c;
    d |= c;
    d ^= c;
    d >>= c;
    d <<= c;

    // TODO : saturated operators ?
}



test "bitwise color" {

    var color: u32 = 0xFF0AFBA2;

    try testing.expectEqual(0xFF, 0x000000FF);
    // X & 0xFF = (get the last 8 bits of X)


    try testing.expectEqual((color>>8*2), 0xFF0A); //move 2 bytes to the right
    try testing.expectEqual(0xFF0A & 0xFF, 0x0A); //get last byte

    try testing.expectEqual((color>>8*2) & 0xFF, 0x0A);

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try testing.expectEqual((color>>8*1), 0xFF0AFB); //move 1 byte to the right
    try testing.expectEqual(0xFF0AFB & 0xFF, 0xFB);

    try testing.expectEqual((color>>8*1) & 0xFF, 0xFB);

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try testing.expectEqual((color>>8*0), 0xFF0AFBA2); //move 0 bytes to the right
    try testing.expectEqual(0xFF0AFBA2 & 0xFF, 0xA2);

    try testing.expectEqual((color>>8*0) & 0xFF, 0xA2);
}

