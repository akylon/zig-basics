//build strings on the fly

const std = @import("std");
const print = std.debug.print;


pub fn foo(comptime s: []const u8) []const u8 {
  return "new"++s;
}



pub fn main() !void {
  print("{s}\n", .{foo("BAR")});


  const txt = "radise ssiflora ris cific thetic";
  var it = std.mem.tokenize(u8, txt, " ");

  // how to do this ???
  //while(it.next()) |word|{
  //  print("{s}\n", .{foo(word)}); // does not work !!!
  //}
   
  while(it.next())|word|{
    var tmp:[100]u8 = undefined;
    const total = try std.fmt.bufPrint(&tmp, "{s}{s}", .{"pa", word});
    print("{s}\n", .{total});
  }
}
