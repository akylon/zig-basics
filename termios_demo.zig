// references :
// https://discord.com/channels/605571803288698900/605572581046747136/910997162358358098
// super tuto on making a text editor, entering raw mode : https://viewsourcecode.org/snaptoken/kilo/

const std = @import("std");
const os = std.os;
const system = std.os.system;
const linux = os.linux;
const print = std.debug.print;

const C_VMIN = 6;
const C_VTIME = 5;



fn enable_raw_mode() !void {

  var termios = try os.tcgetattr(os.STDIN_FILENO);

  termios.iflag &= ~(@as(u16, linux.BRKINT | linux.ICRNL | linux.INPCK | linux.ISTRIP | linux.IXON));
  termios.cflag &= ~(@as(u16, linux.OPOST));
  termios.cflag |=(linux.CS8);
  termios.lflag &=~(@as(u16, linux.ECHO | linux.ICANON | linux.ISIG | linux.ISIG));

  termios.cc[C_VMIN] = 0;
  termios.cc[C_VTIME] = 1;

  try os.tcsetattr(os.STDIN_FILENO, linux.TCSA.FLUSH, termios);
}


fn disable_raw_mode(orig_termios: *os.termios) !void {
    try os.tcsetattr(os.STDIN_FILENO, linux.TCSA.FLUSH, orig_termios.*);
}

pub fn main() !void {
    const stdin = std.io.getStdIn().reader();
    var orig_termios = try os.tcgetattr(os.STDIN_FILENO);

    try enable_raw_mode();
    while(true){
        const byte = stdin.readByte() catch 0; 
        switch(byte) {
            'q' => {
                try disable_raw_mode(&orig_termios);
                std.os.exit(1);
            },
            else => {},
        
        }
    }

    try disable_raw_mode(&orig_termios);
}
