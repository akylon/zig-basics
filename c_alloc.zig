const std = @import("std");
const print = std.debug.print;

pub fn main() !void {
  const allocator = std.heap.c_allocator;

  var foo = allocator.alloc(u8, 3) catch unreachable;
  defer allocator.free(foo);

  std.mem.copy(u8, foo, "FOO");

  print("{s}\n", .{foo});


  foo = allocator.realloc(foo, 6) catch unreachable;
  std.mem.copy(u8, foo, "BAZBAR");

  print("{s}\n", .{foo});
}
