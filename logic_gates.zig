const std = @import("std");
const print = std.debug.print;
const testing = std.testing;















/// ---------------------------------------------------------------------------
pub fn main () void {
    print("{d}\n", .{XOR(0, 0)});
    print("{d}\n", .{XOR(0, 1)});
    print("{d}\n", .{XOR(1, 0)});
    print("{d}\n", .{XOR(1, 1)});
}


fn NOT(a: u1) u1 {
   return if(a == 0) 1 else 0; 
}

fn OR(a: u1, b: u1) u1 {
    return a|b;
}

fn AND(a: u1, b: u1) u1 {
    return a&b;
}

test {
   try testing.expect(NOT(0) == 1); 
   try testing.expect(NOT(1) == 0); 


   try testing.expect(OR(0, 0) == 0);
   try testing.expect(OR(0, 1) == 1);
   try testing.expect(OR(1, 0) == 1);
   try testing.expect(OR(1, 1) == 1);


   try testing.expect(AND(0, 0) == 0);
   try testing.expect(AND(0, 1) == 0);
   try testing.expect(AND(1, 0) == 0);
   try testing.expect(AND(1, 1) == 1);


}




// ---------------------------------------------------------------------------
fn NOR(a: u1, b: u1) u1 {
    return NOT(OR(a, b));
}

fn NAND(a: u1, b: u1) u1 {
    return NOT(AND(a, b));
}

test {
   try testing.expect(NAND(0, 0) == 1);
   try testing.expect(NAND(0, 1) == 1);
   try testing.expect(NAND(1, 0) == 1);
   try testing.expect(NAND(1, 1) == 0);


   try testing.expect(NOR(0, 0) == 1);
   try testing.expect(NOR(0, 1) == 0);
   try testing.expect(NOR(1, 0) == 0);
   try testing.expect(NOR(1, 1) == 0);
}




// ---------------------------------------------------------------------------
// no name ?
fn A(a: u1, b: u1) u1 {
   return (AND(NOT(a), b)); 
}


fn B(a: u1, b: u1) u1 {
   return (AND(a, NOT(b))); 
}

test {
   try testing.expect(A(0, 0) == 0);
   try testing.expect(A(0, 1) == 1);
   try testing.expect(A(1, 0) == 0);
   try testing.expect(A(1, 1) == 0);

   try testing.expect(B(0, 0) == 0);
   try testing.expect(B(0, 1) == 0);
   try testing.expect(B(1, 0) == 1);
   try testing.expect(B(1, 1) == 0);
}






// ---------------------------------------------------------------------------
// build XOR from NAND only !
// build anything from NAND !
fn XOR(a: u1, b: u1) u1 {
    return (
        NAND(
            NAND(a,NAND(a, b))
            ,
            NAND(b,NAND(a, b))
        )
    );
}

test {
    try testing.expect(XOR(0, 0) == 0);
    try testing.expect(XOR(0, 1) == 1);
    try testing.expect(XOR(1, 0) == 1);
    try testing.expect(XOR(1, 1) == 0);
}
