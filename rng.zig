// inpired by : https://discord.com/channels/605571803288698900/1105463570856169482/1105463570856169482
const std = @import("std");
const print = std.debug.print;
const testing = std.testing;


/// returns a number between min and max (both included)
fn get_random_number(comptime T: type, min: T, max: T) !T {
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        try std.os.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    });

    const rand = prng.random();
    return rand.intRangeAtMost(T, min, max);
}


test "generate random number" {
    const nb = try get_random_number(u8, 0, 1);
    try testing.expect(nb == 0 or nb == 1);
    
    const nb2 = try get_random_number(u16, 256, 257);
    try testing.expect(nb2 == 256 or nb2 == 257);
}