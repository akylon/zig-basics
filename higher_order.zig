// see here : https://ziglang.org/documentation/master/#Functions
const std = @import("std");
const testing = @import("std").testing;

const fnType = *const fn (i8, i8) i8;

fn add(a: i8, b: i8) i8 {
    return a + b;
}


fn sub(a: i8, b: i8) i8 {
    return a - b;
}

fn doOp(f: fnType, a: i8, b: i8) i8 {
    return f(a, b);
}


test {
    try testing.expectEqual(doOp(add, 8, 4), 12);
    try testing.expectEqual(doOp(sub, 8, 4), 4);
}
