const std = @import("std");
const print = std.debug.print;
const testing = std.testing;

// stacks have 2 operations :
// push
// pop
// first in first out (fifo)

test {
    var stack = std.ArrayList(u8).init(testing.allocator);
    defer stack.deinit();

    try stack.append(72); // push operation;
    try stack.append(24);

    try testing.expect(stack.pop() == 24);
    try testing.expect(stack.pop() == 72);
}
