// from https://discord.com/channels/605571803288698900/1134451780416196689/1134451780416196689
const std = @import("std");
const os = std.os;
const sys = os.system;

pub fn main() !void {
    const stdin_fd = std.io.getStdIn().handle;
    const original_attrs = try os.tcgetattr(stdin_fd);

    var new_attrs = original_attrs;
    new_attrs.lflag &= ~@as(sys.tcflag_t, sys.ECHO | sys.ICANON);

    try os.tcsetattr(stdin_fd, .FLUSH, new_attrs);
    defer os.tcsetattr(stdin_fd, .FLUSH, original_attrs) catch {};

    // while(true){}
}
