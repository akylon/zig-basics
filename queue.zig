const std = @import("std");
const print = std.debug.print;
const testing = std.testing;

// use ArrayList as Queue :
//
// * add element to end of queue
// * remove element from head of queue
// * isEmpty()
//
// last in first out : lifo

test {
    var queue = std.ArrayList(u8).init(testing.allocator);
    defer queue.deinit();

    try queue.append(3);
    try queue.append(14);

    try testing.expectEqual(queue.items.len, 2);

    try testing.expectEqual(queue.orderedRemove(0), 3);
    try testing.expectEqual(queue.orderedRemove(0), 14);

    try testing.expectEqual(queue.items.len, 0);
}
