const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;


pub fn main() !void {
  //allocator boilerplate
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer _ = gpa.deinit();
  
  //let us define some "strings" (in zig []u8) to manipulate :
  var foo = allocator.alloc(u8, 3) catch unreachable;
  defer allocator.free(foo);
  std.mem.copy(u8, foo, "FOO");
  
  var bar = allocator.alloc(u8, 3) catch unreachable;
  defer allocator.free(bar);
  std.mem.copy(u8, bar, "BAR");

  var foobar = allocator.alloc(u8, 6) catch unreachable;
  defer allocator.free(foobar);
  std.mem.copy(u8, foobar, "FOOBAR");


  //lst is our "array of strings"
  var lst: [][]u8 = undefined;

  lst = allocator.alloc([]u8, 2) catch unreachable;
  defer allocator.free(lst);

  lst[0] = foo;
  lst[1] = bar;
  


  //we want to add 1 item to our lst.

  //first, we will create a temporary buffer called tmp 
  var tmp = allocator.alloc([]u8, lst.len) catch unreachable;
 
  //we then copy all items from lst to tmp
  for(lst)|_, i|{
    tmp[i] = lst[i]; 
  }

  //allocate space for new lst
  lst = allocator.realloc(lst, lst.len+1) catch unreachable;

  //transfer all items from tmp to lst
  for(tmp)|_, i|{
    lst[i] = tmp[i];
  }

  //free temporary buffer
  allocator.free(tmp);
  
  //add our new item at the end of lst
  lst[lst.len-1] = foobar;
  
  for(lst)|item|{
    print("{s}\n", .{item});
  }
}
