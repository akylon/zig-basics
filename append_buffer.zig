// see https://discord.com/channels/605571803288698900/1056901619465334836/1056901619465334836 for more info ...
// thanks to @QuantumDeveloper on discord

// free and realloc ignore 0 length slices


const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;

const AppendBuffer = struct {
  allocator: Allocator,
  buffer: []u8 = &[0]u8{}, //initialize buffer to zero length size

  fn init(allocator: Allocator) AppendBuffer {
    return AppendBuffer {
      .allocator = allocator,
    };
  }

  fn append(self: *AppendBuffer, new: []const u8 ) !void {
    const len = self.buffer.len;
    self.buffer = try self.allocator.realloc(self.buffer, self.buffer.len+new.len); 
    std.mem.copy(u8, self.buffer[len..self.buffer.len], new);
  }

  fn deinit(self: *AppendBuffer) void {
    if(self.buffer.len != 0) { 
      self.allocator.free(self.buffer);
    }
  }
};


test "append_buffer" {

   var s = AppendBuffer.init(testing.allocator); 
   defer s.deinit();

   try s.append("foo");
   try testing.expectEqualSlices(u8, s.buffer, "foo");

   try s.append("bar");
   try testing.expectEqualSlices(u8, s.buffer, "foobar");
}
