const std = @import("std");
const print = std.debug.print;
const StringHashMap = std.StringHashMap;


pub fn main() !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer {
    const leaked = gpa.deinit();
    if(leaked) @panic("leaked");
  }

  var map = StringHashMap([]const u8).init(allocator);
  defer map.deinit();

  try map.put("foobar", "FOOBAR");
  print("{s}\n", .{map.get("foobar").?});
}
