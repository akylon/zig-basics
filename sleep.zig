//print x every second 10 times

const std = @import("std");
const print = std.debug.print;

pub fn main() void {

  const asleep = std.math.pow(u64 ,10, 9);  // <=> 10^9 

  var x : u8 = 0;
  while(x<10) : (x+=1){
    print("{d}\n", .{x});
    std.time.sleep(asleep); //std.time.sleep(nanoseconds);
  }
}
