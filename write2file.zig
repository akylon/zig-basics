//write []u8 to file demo


const std = @import("std");
const print = std.debug.print;


pub fn write2file(filename: []const u8, message : []const u8) !void {
  const file = try std.fs.cwd().createFile(filename, .{.read = true},);   
  try file.writeAll(message);
  defer file.close();
}


pub fn main() !void {
  try write2file("foo2.txt", "Hello (write2File) world !");
}
