// taken from https://github.com/ziglang/zig/blob/master/lib/std/linked_list.zig

const std = @import("std");
const print = std.debug.print;
const LinkedList = std.SinglyLinkedList(u32);

pub fn main() !void {
  var list = LinkedList{}; 

  var one = LinkedList.Node{ .data=1 };
  var two = LinkedList.Node{ .data=2 };
  var three = LinkedList.Node{ .data=3 };
  var four = LinkedList.Node{ .data=4 };

  list.prepend(&two);
  two.insertAfter(&three);
  list.prepend(&one);
  three.insertAfter(&four);
  print("{any}\n", .{list});

  var it = list.first;
  var index: u32 = 1;
  while(it) |node| : (it = node.next){
    print("{d}\n", .{node.data});
    index+=1;
  }
}
