const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const testing = std.testing;

fn int2BinArray(nb: u8) [3]u1{
   var arrBins: [3]u1 = undefined;
   arrBins[0] = @truncate(nb>>2);
   arrBins[1] = @truncate(nb>>1);
   arrBins[2] = @truncate(nb>>0);
   return arrBins;
}


test {
    try testing.expect(std.mem.eql(u1, &int2BinArray(7), &[_]u1{1, 1, 1}));
    try testing.expect(std.mem.eql(u1, &int2BinArray(4), &[_]u1{1, 0, 0}));
    try testing.expect(std.mem.eql(u1, &int2BinArray(0), &[_]u1{0, 0, 0}));
}
