const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;
const assert = std.debug.assert;
const print = std.debug.print;


/// a generic stack datatype
pub fn Stack (comptime T: type) type {
    return struct {
        const Self = @This();
        allocator: Allocator,
        items: []T,
        size: usize,

        pub fn init(allocator: Allocator) !Self {
          return .{
            .items = try allocator.alloc(T, 2),
            .allocator = allocator,
            .size = 0,
          };
        }

        pub fn deinit(self: Self) void {
          self.allocator.free(self.items);
        }

        pub fn grow(self: *Self) !void {
          print("growing...\n", .{});
          self.items = try self.allocator.realloc(self.items, self.items.len+2);
        }

        pub fn shrink(self: *Self) !void {
          print("shrinking...\n", .{});
          self.items = self.allocator.realloc(self.items, self.items.len-2);
        }

        pub fn push(self: *Self, item: T) !void {
            if (self.size == self.items.len) {
                try grow(self); 
            }
            self.items[self.size] = item;
            self.size += 1;
        }

        pub fn pop(self: *Self) ?T {
            if (self.size == 0) return null;
            print("self.size = {d}\n", .{self.size});
            var tmp = self.items[self.size-1];
            self.size -= 1;
            return tmp;
        }
    };
}

test "stack allocated" {
    const allocator = testing.allocator;
    var stack = try Stack([]u8).init(allocator);
    defer stack.deinit();

    try testing.expect (stack.pop() == null);
    
    var foo1 = try allocator.alloc(u8, 6);
    std.mem.copy(u8, foo1, "foobar");
    defer allocator.free(foo1);

    var foo2 = try allocator.alloc(u8, 6);
    std.mem.copy(u8, foo2, "doobar");
    defer allocator.free(foo2);

    var foo3 = try allocator.alloc(u8, 6);
    std.mem.copy(u8, foo3, "goobar");
    defer allocator.free(foo3);

    try stack.push(foo1);

    try testing.expect(std.mem.eql(u8, stack.pop().?, "foobar"));


    try stack.push(foo1);
    try stack.push(foo2);
    try stack.push(foo3);

    try testing.expect(std.mem.eql(u8, stack.pop().?, "goobar"));
    try testing.expect(std.mem.eql(u8, stack.pop().?, "doobar"));
    try testing.expect(std.mem.eql(u8, stack.pop().?, "foobar"));
    try testing.expect(stack.pop() == null);

    try stack.push(foo1);
    try stack.push(foo2);
    try stack.push(foo3);


    try testing.expect(std.mem.eql(u8, stack.pop().?, "goobar"));
    try testing.expect(std.mem.eql(u8, stack.pop().?, "doobar"));
    try testing.expect(std.mem.eql(u8, stack.pop().?, "foobar"));
    try testing.expect(stack.pop() == null);
}

