const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;

/// stack datastructure 
/// using Nodes to store data
/// bad. see stack_v3.zig
pub fn Stack(comptime T: type) type {
    return struct {
        const Self = @This();
        const Node = struct {
            data: T,
            next: ?*Node,  
        };  

        allocator: Allocator,
        head: ?*Node,
        size: usize,

        pub fn init (allocator: Allocator) Self {
            return .{
                .allocator = allocator,
                .head = null, 
                .size = 0,
            }; 
        }

        pub fn push (self: *Self, value: T) !void {
            var new_node = try self.allocator.create(Node);
            new_node.data = value;
            new_node.next = null;

            new_node.next = self.head;
            self.head = new_node;

            self.size += 1; 
        }

        pub fn pop (self: *Self) ?T {
            if(self.head) |unwrapped| {
                const node_data = unwrapped.data;
                self.head = unwrapped.next;

                self.allocator.destroy(unwrapped);
                self.size -= 1;

                return node_data;
            }
            return null; 
        }
    };
}




test "Stack" {
    var stack = Stack(u8).init(testing.allocator);

    try testing.expectEqual(stack.size, 0);
    try testing.expectEqual(stack.pop(), null);

    try stack.push(35);
    try testing.expectEqual(stack.size, 1);
    try testing.expectEqual(stack.pop(), 35);
    try testing.expectEqual(stack.pop(), null);

    try stack.push(23);
    try stack.push(24);
    try stack.push(25);
    try stack.push(26);
    try stack.push(27);

    try testing.expectEqual(stack.size, 5);

    try testing.expectEqual(stack.pop(), 27);
    try testing.expectEqual(stack.pop(), 26);
    try testing.expectEqual(stack.pop(), 25);
    try testing.expectEqual(stack.pop(), 24);
    try testing.expectEqual(stack.pop(), 23);

    try testing.expectEqual(stack.size, 0);

    try testing.expectEqual(stack.pop(), null);
    try testing.expectEqual(stack.pop(), null);
    try testing.expectEqual(stack.pop(), null);
}
