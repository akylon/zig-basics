const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;
const assert = std.debug.assert;



/// a generic stack datatype
/// in zig, this is the way to implement generics
/// uses a slice internally to store datatype
/// bad. See stack_v3.zig
pub fn Stack (comptime T: type) type {

    return struct {
        const Self = @This();
        allocator: Allocator,
        stack: []T,
        index: usize = 0,
        growth_factor: u8 = 10,

        pub fn init(allocator: Allocator) !Self {
          return .{
            .stack = try allocator.alloc(T, 10), // allow enough for 10 items initially
            .allocator = allocator,
          };
        }

        pub fn deinit(self: Self) void {
          self.allocator.free(self.stack);
        }

        pub fn size(self: Self) usize {
            return self.index;
        }

        pub fn grow(self: *Self) !void {
          // print("growing...\n", .{});
          self.stack = try self.allocator.realloc(self.stack, self.stack.len+self.growth_factor);
        }

        pub fn shrink(self: *Self) !void {
          // print("shrinking...\n", .{});
          self.stack = try self.allocator.realloc(self.stack, self.stack.len-self.growth_factor);
        }


        pub fn push(self: *Self, value: T) !void {
          self.stack[self.index] = value;
          // print("pushed {d}\n", .{value});
          self.index+=1;  
          if(self.index==self.stack.len){
            try self.grow();
          }
        }

        pub fn pop(self: *Self) !?T {

            if(self.index == 0) {
                return null;
            }

            const returned = self.stack[self.index-1];
            self.index-=1;

            if (self.stack.len>=self.growth_factor+1 and self.index==self.stack.len-self.growth_factor-1) {
                try self.shrink();
            }
            return returned;
        }
        
    };
}


test "stack" {
    var stack = try Stack(u8).init(testing.allocator);
    defer stack.deinit();

    try stack.push(1);
    try stack.push(0);

    try testing.expectEqual(stack.size(), 2);

    try testing.expectEqual(try stack.pop(), 0);
    try testing.expectEqual(try stack.pop(), 1);
    
    try testing.expectEqual(stack.size(), 0);
    // stack now empty

    try stack.push(112);

    try testing.expectEqual(try stack.pop(), 112);
    try testing.expectEqual(try stack.pop(), null);


    try stack.push(69);
    try testing.expect( (try stack.pop()).? == 69); // note bizarre syntax here
} 

