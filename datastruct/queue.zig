// simple queue in zig
// adapted from the code here : https://ziglang.org/learn/samples/ 
// (see queue.zig)



const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;

pub fn Queue(comptime T: type) type {
    return struct {
        const Self = @This();

        const Node = struct {
            data: T,
            next: ?*Node, 
        };
        allocator: Allocator,
        start: ?*Node,
        end: ?*Node,

        pub fn init(allocator: Allocator) Self {
            return Self {
                .allocator = allocator,
                .start = null,
                .end = null,
            }; 
        }
        
        pub fn enqueue(self: *Self, value: T) !void {
            const node = try self.allocator.create(Node);
            node.* = .{.data = value, .next = null };
            if(self.end)|end|{
                end.next = node;    
            }    
            else {
                self.start = node; 
            }
            self.end = node;
        }

        pub fn dequeue(self: *Self) ?T {
            const start = self.start orelse return null;
            defer self.allocator.destroy(start);
            if(start.next)|next|{
                self.start = next; 
            }
            else {
                self.start = null;
                self.end = null;
            }
            return start.data;
        }
    };
}

test "queue" {
    var int_queue = Queue(u8).init(testing.allocator);

    try int_queue.enqueue(25);
    try int_queue.enqueue(50);
    try int_queue.enqueue(75);
    try int_queue.enqueue(100);

    try testing.expectEqual(int_queue.dequeue(), 25);
    try testing.expectEqual(int_queue.dequeue(), 50);
    try testing.expectEqual(int_queue.dequeue(), 75);
    try testing.expectEqual(int_queue.dequeue(), 100);
    try testing.expectEqual(int_queue.dequeue(), null);

    try int_queue.enqueue(33);
    try int_queue.enqueue(44);


    try testing.expectEqual(int_queue.dequeue(), 33);
    try testing.expectEqual(int_queue.dequeue(), 44);
    try testing.expectEqual(int_queue.dequeue(), null);

}
