//  extracted from standard library
//  on std, it is called "TailQueue"
//  see following links : 
//  https://ziglang.org/documentation/master/std/src/std/linked_list.zig.html#L160
//  https://github.com/ziglang/zig/blob/master/lib/std/linked_list.zig


const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const testing = std.testing;

pub fn DoublyLinkedList(comptime T: type) type {
    return struct {
        const Self = @This();
        pub const Node = struct {
            prev: ?*Node = null,
            next: ?*Node = null,
            data: T,
        }; 
        first: ?*Node = null,
        last: ?*Node = null,
        len: usize = 0,

        pub fn insertAfter(list: *Self, node: *Node, new_node: *Node) void {
            new_node.prev = node;
            if(node.next) |next_node| {
                new_node.next = next_node;
                next_node.prev = new_node; 
            } else {
                new_node.next = null;
                list.last = new_node;
            }
            node.next = new_node;

            list.len+=1;
        }

        pub fn insertBefore(list: *Self, node: *Node, new_node: *Node) void {
            new_node.next = node;
            if(node.prev)|prev_node|{
                new_node.prev = prev_node;
                prev_node.next = new_node;
            } else {
                new_node.prev = null; 
                list.first = new_node;
            }
            node.prev = new_node;
            list.len += 1;
        }


        pub fn concatByMoving(list1: *Self, list2: *Self) void {
            const l2_first = list2.first orelse return;
            if(list1.last)|l1_last|{
                l1_last.next = list2.first;
                l2_first.prev = list1.last;
                list1.len += list2.len; 
            } else {
                list1.first = list2.first;
                list1.len = list2.len; 
            }
            list1.last = list2.last;
            list2.first = null;
            list2.last = null;
            list2.len = 0;
        }

        pub fn append(list: *Self, new_node: *Node) void {
            if(list.last) |last| {
               list.insertAfter(last, new_node); 
            } else {
                list.prepend(new_node); 
            }
        }

        pub fn prepend(list: *Self, new_node: *Node) void { //bug somewhere in this scope

            if(list.first)|first|{
                // insert before first
                list.insertBefore(first, new_node); 
            } else {
                list.first = new_node;
                list.last = new_node;
                new_node.prev = null;
                new_node.next = null;
                list.len = 1;
            }
        }


        pub fn remove(list: *Self, node: *Node) void {
            if(node.prev) |prev_node| {
                prev_node.next = node.next; 
            } else {
                list.first = node.next; 
            } 

            if(node.next)|next_node|{
                next_node.prev = node.prev; 
            } else {
                list.last = node.prev; 
            }

            list.len -= 1;
            assert(list.len == 0 or (list.first != null and list.last != null));
        }

        pub fn pop(list: *Self) ?*Node {
            const last = list.last orelse return null;
            list.remove(last);
            return last; 
        }

        pub fn popFirst(list: *Self) ?*Node {
            const first = list.first orelse return null;
            list.remove(first);
            return first; 
        }
    };
}

pub fn traverse_forward(list: *DoublyLinkedList(u32) ) void {
        // traverse forwards
        var it = list.*.first;
        var index: u32 = 1;
        while(it)|node| : (it = node.next) {
            print("{d} ", .{node.data});
            index += 1;
        }
        print("\n\n", .{});
}

pub fn traverse_backward(list: *DoublyLinkedList(u32) ) void {

        var it = list.*.last;
        var index: u32 = 1;
        while(it)|node| : (it = node.prev) {
            print("{d} ", .{node.data});
            index += 1;
        }
        print("\n\n", .{});
}





test "dll" {
    const L = DoublyLinkedList(u32);
    var list = L{};

    var n1 = L.Node{ .data=1};
    var n2 = L.Node{ .data=2};
    var n3 = L.Node{ .data=3};
    var n4 = L.Node{ .data=4};
    var n5 = L.Node{ .data=5};

    list.append(&n2);
    list.append(&n5);

    list.prepend(&n1);


    list.insertBefore(&n5, &n4);
    list.insertAfter(&n2, &n3);

    try testing.expectEqual(list.popFirst().?, &n1);

    try testing.expectEqual(list.pop().?, &n5);

    // list.remove(&n3);
}
