const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;
const print = std.debug.print;
const assert = std.debug.assert;





/// Stack datastucture
/// uses linked list
pub fn Stack (comptime T: type) type {
    return struct {
        const Self = @This();
        const Node = struct {
            value: T,
            next: ?*Node,
        };

        allocator: Allocator, 
        head: ?*Node,
        size: usize,

        fn init(allocator: Allocator) Self {
            return .{
                .allocator = allocator,
                .head = null, 
                .size = 0,
            };
        }

        fn deinit(self: Self) void {
            freeAll(self);
        }

        fn freeAll(self: Self) void {
            var item = self.head;
            while(item) |node| {
                item = node.next;
                self.allocator.destroy(node); 
            }
        }

        fn push(self: *Self, node: T) !void {
            var new_node = try self.allocator.create(Node);
            new_node.value = node;

            new_node.next = self.head;
            self.head = new_node;
            self.size += 1;
        }

        fn pop(self: *Self) ?T {
            if (self.head) |unwrapped| {
                var tmp = unwrapped.value;
                self.head = unwrapped.next;
                self.allocator.destroy(unwrapped);
                self.size -= 1;
                return tmp;
            }
            return null;
        }
    };
}


test "stack-allocated objects" {

    const allocator = testing.allocator;
    var stack = Stack(u8).init(allocator);
    defer stack.deinit();

    try testing.expectEqual(stack.head, null);

    var foo1: u8 = 43;
    var foo2: u8 = 54;
    var foo3: u8 = 65;

    try stack.push(foo1);
    try stack.push(foo2);
    try stack.push(foo3);
}




test "heap-allocated objects" {
    const allocator = testing.allocator;

    var stack = Stack([]u8).init(allocator);
    defer stack.deinit();

    try testing.expectEqual(stack.head, null);

    var foo1 = try allocator.alloc(u8, 6);
    std.mem.copy(u8, foo1, "foobar");
    defer allocator.free(foo1);

    var foo2 = try allocator.alloc(u8, 6);
    std.mem.copy(u8, foo2, "doobar");
    defer allocator.free(foo2);

    var foo3 = try allocator.alloc(u8, 6);
    std.mem.copy(u8, foo3, "goobar");
    defer allocator.free(foo3);

    var foo4 = try allocator.alloc(u8, 6);
    std.mem.copy(u8, foo4, "hoobar");
    defer allocator.free(foo4);

    try stack.push(foo1);
    try testing.expect(stack.head != null);

    try testing.expect(std.mem.eql(u8, stack.pop().?, foo1));
    try testing.expectEqual(stack.pop(), null);
    try testing.expectEqual(stack.pop(), null);

    try stack.push(foo2);
    try stack.push(foo3);
    try stack.push(foo4);

    try testing.expect(stack.head != null);

    try testing.expectEqual(stack.pop().?, foo4); // a pointer

    try testing.expect(std.mem.eql(u8, foo1, "foobar"));
    try testing.expect(std.mem.eql(u8, foo2, "doobar"));
    try testing.expect(std.mem.eql(u8, foo3, "goobar"));
    try testing.expect(std.mem.eql(u8, foo4, "hoobar"));
}
