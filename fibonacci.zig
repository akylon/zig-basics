// fibonacci sequence exercises

const std = @import("std");
const print = std.debug.print;
const testing = std.testing;


// classic fibonacci
pub fn fibonacci(index: u64) u64 {
    if(index == 0 or index == 1) {
        return index;
    }
    return fibonacci(index-2)+fibonacci(index-1); 
}


// fibonacci without recursion
pub fn fibonacci2(index: u64) u64 {
    if(index == 0) {
        return 0; 
    } else if(index == 1) {
        return 1; 
    } else { // here, index >= 2
        var sum: u64 = 0;
        var x: u64 = 1;
        var n_minus_1: u64 = 1;
        var n_minus_2: u64 = 0;

        while(x<index):(x+=1) {
            sum = n_minus_1 + n_minus_2;
            n_minus_2 = n_minus_1;
            n_minus_1 = sum;
        }

        return sum;
    }
}



// return an array of n fibonacci sequence numbers
pub fn generate_fibonacci_seq(comptime n: u64) [n]u64{
    var lst: [n]u64 = undefined ;

    lst[0] = 0;
    lst[1] = 1;

    var i: u64 = 2;
    while(i<lst.len):(i+=1){
        lst[i] = lst[i-1]+lst[i-2];
    }
    return lst;
}


test "fibonacci" {
    const nb_iterations = 20;
    const fibonacci_seq = generate_fibonacci_seq(nb_iterations);
    var i: u32 = 0;
    while(i<nb_iterations):(i+=1){
        try testing.expect(fibonacci_seq[i] == fibonacci(i));    
        try testing.expect(fibonacci_seq[i] == fibonacci2(i));    
    }
}
