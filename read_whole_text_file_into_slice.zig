const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;
const MAXSIZE = 1*1024*1024;


pub fn load_text_file (allocator: Allocator, filename : []const u8) ![]u8 {
   var buffer: []u8 = &[0]u8{};
   var index: usize = 0;


   const file = try std.fs.cwd().openFile(filename, .{}); 
   defer file.close();

   while (try file.reader().readUntilDelimiterOrEofAlloc(allocator, '\n', MAXSIZE)) |line| {

     index = buffer.len;
     buffer = try allocator.realloc(buffer, buffer.len+line.len);
     std.mem.copy(u8, buffer[index..buffer.len], line);


     allocator.free(line);
  }
  return buffer;

}


pub fn main() !void {

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();


    const filename = "input.txt";
    var file_content = try load_text_file(allocator, filename);
    defer allocator.free(file_content);

    print("{s} {d} \n", .{file_content, file_content.len});
}
