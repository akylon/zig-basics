// Pass command line arguments demo
// `./args foo bar`



const std = @import("std");
const print = std.debug.print;


pub fn main() !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer _ = gpa.deinit();


  const args = try std.process.argsAlloc(allocator);
  defer std.process.argsFree(allocator, args);

  for(args) |arg, i| {
    print("arg {d} ->{s}\n", .{i, arg});
  }
}
