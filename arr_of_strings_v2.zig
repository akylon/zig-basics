const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;


const ArrString = struct {
  allocator: Allocator,
  buffer: ?[][]u8 = undefined,
  
  pub fn init(self: *ArrString) void {
    self.buffer = null;
  }

  pub fn add(self: *ArrString, input: []const u8) void {
    if(self.buffer == null){
      self.buffer = self.allocator.alloc([]u8, 1) catch unreachable;
      var tmp = self.allocator.alloc(u8, input.len) catch unreachable; 
      std.mem.copy(u8, tmp, input);
      self.buffer.?[0] = tmp;
    }
    else {
      var tmp = self.allocator.alloc([]u8, self.buffer.?.len) catch unreachable;
      for(self.buffer.?)|_, i|{
        tmp[i] = self.buffer.?[i]; 
      }     
      self.buffer = self.allocator.realloc(self.buffer.?, tmp.len+1) catch unreachable;
      
      for(tmp)|_, i|{
        self.buffer.?[i]=tmp[i];
      }
      self.buffer.?[self.buffer.?.len-1] = self.allocator.alloc(u8, input.len) catch unreachable;
      std.mem.copy(u8, self.buffer.?[self.buffer.?.len-1], input); 
     
      self.allocator.free(tmp);

    }
  }

  pub fn deinit(self: *ArrString) void {
    if(self.buffer!=null){
      for(self.buffer.?)|item|{
        self.allocator.free(item);
      }
      self.allocator.free(self.buffer.?);
    }
  }

  pub fn stringPrint(self: *ArrString) void {
    for(self.buffer.?)|item|{
      print("{s}\n", .{item});
    }   
  }
};


pub fn main() !void {

  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer _ = gpa.deinit();

  var s1 = String{
    .allocator = allocator,
  };
  s1.init();
  defer s1.deinit();

  s1.add("BAR");
  s1.stringPrint();
  
  print("{s}\n", .{"------------------"});

  s1.add("FOO"); 
  s1.stringPrint();

  print("{s}\n", .{"------------------"});
  s1.add("FOOBAR");
  s1.stringPrint();
}
