const std = @import("std");
const print = std.debug.print;




pub fn main() !void {

    const int_as_string = "897";
    print("{d}\n", .{try std.fmt.parseInt(u32, int_as_string, 10) + 3 });

}
