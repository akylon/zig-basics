const print = @import("std").debug.print;


pub fn increment(num: *u8) void {
  num.* += 1;
}


pub fn main() void {
  var x: u8 = 0;

  print("{d}\n", .{&x}); //address of x
  print("{d}\n", .{x}); //value of x = 0

  increment(&x);

  print("{d}\n", .{&x}); //adress of x. Stays the same.
  print("{d}\n", .{x});  //value of x = 1
}

