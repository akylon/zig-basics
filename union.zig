// bare unions demo

const std = @import("std");
const print = std.debug.print; 

const Zero  = union {
    nb: u8,
    letters: []const u8,
};

// unions are a set of possible values. 
// Only 1 value can be active at the same time

// tagged unions : eligible to use in a switch

pub fn main() !void {
    var zero = Zero {.nb=0};
    print("{d}\n", .{zero.nb});

    // print("{s}\n", .{zero.letters}); // not permitted

    zero = Zero{.letters="zero"};
    print("{s}\n", .{zero.letters});
}
