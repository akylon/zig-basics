const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;


const Token = struct {
  content: ?[]u8,
  allocator: Allocator,

  pub fn init(allocator: Allocator) *Token {
    var tmp = allocator.create(Token) catch unreachable;  
    tmp.* = Token{.content = null, .allocator = allocator,};
    return tmp;
  }

  pub fn setContent(self: *Token, x: []const u8) void {
    if(self.content != null) self.allocator.free(self.content.?);
    self.content = self.allocator.alloc(u8, x.len) catch unreachable; 
    std.mem.copy(u8, self.content.?, x);
  }

  pub fn deinit(self: *Token) void {
    if(self.content != null) self.allocator.free(self.content.?);
    self.allocator.destroy(self);
  }
};


pub fn main() !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();
  defer _ = gpa.deinit();

  var t = Token.init(allocator);
  defer t.deinit();

  t.setContent("foo");
  print("{s}\n", .{t.content.?});

  t.setContent("foobar");
  print("{s}\n", .{t.content.?});
}
