// see : std.SinglyLinkedList 
// https://ziglang.org/documentation/master/std/#A;std:SinglyLinkedList

const std = @import("std");
const print = std.debug.print;



const Node = struct {
    data: u8,
    next : ?*Node = null,
};


pub fn browse(node: ?*Node) void {

    if(node == null)  {
        return;
    }

    else {
        print("{d}\n", .{node.?.*.data}); 
        browse(node.?.*.next);
    }

}


pub fn main () void {
    print("Node datatype\n", .{});

    var n0 = Node{ .data = 0};
    var n1 = Node{ .data = 1};

    n0.next = &n1;

    browse(&n0);
}
