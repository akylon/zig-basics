const std = @import("std");
const print = std.debug.print;
const MAXSIZE = 1*1024*1024;


pub fn writeBytesToFile(filename: []const u8, bytes: []const u8) !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer {
    const leaked = gpa.deinit();
    if(leaked) @panic("leaked");
  }

  const file = std.fs.cwd().openFile(filename, .{}) catch null;
  

  if(file == null){ 
    print("file {s} doesn't exist, creating it and writing bytes.\n", .{filename});

    const file2 = try std.fs.cwd().createFile(filename, .{.read=true},);
    defer file2.close();
    for(bytes)|byte|{
      try file2.writer().writeByte(byte);
    }
  }

  else {
    print("file {s} exists, appending ...\n", .{filename});

    var tmp:[][]const u8 = undefined;
    tmp = allocator.alloc([]u8, 1) catch unreachable;

    while(try file.?.reader().readUntilDelimiterOrEofAlloc(allocator, '\n', MAXSIZE)) |line| {
      if(tmp.len == 1){
        tmp[0] = line;
      }
      else {
        var tmp2: [][]const u8 = allocator.alloc([]u8, tmp.len) catch unreachable;
        for(tmp)|_, i|{
          tmp2[i] = tmp[i];
        }

        tmp = allocator.realloc(tmp, tmp.len+1) catch unreachable;
        for(tmp2)|_, i|{
          tmp[i] = tmp2[i]; 
        }
        tmp[tmp.len-1] = line;
        allocator.free(tmp2);
      }
    }
    file.?.close();


    const file2 = try std.fs.cwd().createFile(filename, .{.read=true},);
    defer file2.close();
    
    for(tmp)|item|{
      _ = try file2.writer().write(item);
    }

    for(bytes)|byte|{
      _ = try file2.writer().writeByte(byte);
    }

    for(tmp)|item|{
      allocator.free(item);
    }
    allocator.free(tmp);
  }
}




pub fn main() !void {
  const arr = [_]u8 {0xFF, 0xAA};
  try writeBytesToFile("bar.txt", &arr);
}
