const print = @import("std").debug.print;

const Point = struct {
  x: u8,
  y: u8,

  fn swap(self: *Point) void {
    const tmp = self.x;
    self.x = self.y;
    self.y = tmp;
  }
};



pub fn main() void {

  var p1 = Point { 
    .x=2,
    .y=3
  };

  print("{d}\n", .{p1.x}); //2
  print("{d}\n", .{p1.y}); //3

  p1.swap();

  print("{d}\n", .{p1.x}); //3
  print("{d}\n", .{p1.y}); //2
}
