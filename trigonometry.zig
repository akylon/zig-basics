// reference : 
// https://en.wikipedia.org/wiki/Trigonometry

const std = @import("std");
const print = std.debug.print;
const testing = std.testing;



const cos = std.math.cos;
const sin = std.math.sin;
const tan = std.math.tan;
const pi = std.math.pi;
const radiansToDegrees = std.math.radiansToDegrees;
const degreesToRadians = std.math.degreesToRadians;

const sqrt = std.math.sqrt;

// const CIRCLE_CIRCUMFERENCE = 2*pi*R;
// pi/2 radians == 90°
// pi radians == 180°

pub fn main() void {
    print("trigonometry stuff \n", .{});
    const float1 = 0.1234561111;
    const float2 = 0.123457;
    const float3 = 0.1234561112;


    print("{}\n", .{std.math.approxEqRel(f32, float1, float2, 1e-6)}); //false
    print("{}\n", .{std.math.approxEqRel(f32, float1, float3, 1e-6)}); //true

}


test "radians" {

    try testing.expectApproxEqAbs(radiansToDegrees(f32, pi), 180, 1e-6);
    try testing.expectApproxEqAbs(radiansToDegrees(f32, pi/2.0), 90, 1e-6);
    try testing.expectApproxEqAbs(radiansToDegrees(f32, pi/3.0), 60, 1e-6);
    try testing.expectApproxEqAbs(radiansToDegrees(f32, pi/4.0), 45, 1e-6);
    try testing.expectApproxEqAbs(radiansToDegrees(f32, pi/6.0), 30, 1e-6);

    try testing.expectApproxEqAbs(degreesToRadians(f32, 90), pi/2.0, 1e-6);
    try testing.expectApproxEqAbs(degreesToRadians(f32, 60), pi/3.0, 1e-6);
    try testing.expectApproxEqAbs(degreesToRadians(f32, 45), pi/4.0, 1e-6);
    try testing.expectApproxEqAbs(degreesToRadians(f32, 30), pi/6.0, 1e-6);

}



test "cosine" {
    // argument of cos() needs to be in radians !!!!!

    //cos(pi) == cos(180°) == -1
    try testing.expect(cos(pi) == -1);

    // cos(2*pi) == cos(0°) == 1
    try testing.expect(cos(2*pi) == 1);

    // cos(pi/2) == cos(90°) == 0 
    try testing.expectApproxEqAbs(cos(degreesToRadians(f32, 90)), @as(f32, 0), 1e-6);

    // cos(pi/3) == cos(60°) == .5 
    try testing.expectApproxEqAbs(cos(degreesToRadians(f32, 60)), 0.5, 1e-6);

    // cos(pi/4) == cos(45°) == sqrt(2)/2
    try testing.expectApproxEqAbs(cos(degreesToRadians(f32, 45)), sqrt(2.0)/2.0, 1e-6);

    // cos(pi/6) == cos(30°) == sqrt(3)/2
    try testing.expectApproxEqAbs(cos(degreesToRadians(f32, 30)), sqrt(3.0)/2.0, 1e-6);
}

test "sine" {
    //sin(pi) == sin(180°) == 0 
    // try testing.expect(sin(pi) == 0);
    try testing.expectApproxEqAbs(@as(f32, sin(pi)), 0.0, 1e-6);

    // sin(2*pi) == sin(0°) == 0
    try testing.expectApproxEqAbs(@as(f32, sin(2*pi)), 0.0, 1e-6);

    // sin(pi/2) == sin(90°) == 1
    try testing.expectApproxEqAbs(sin(degreesToRadians(f32, 90)), @as(f32, 1), 1e-6);

    // sin(pi/3) == sin(60°) == sqrt(3)/2
    try testing.expectApproxEqAbs(sin(degreesToRadians(f32, 60)), sqrt(3.0)/2.0, 1e-6);

    // sin(pi/4) == sin(45°) == sqrt(2)/2
    try testing.expectApproxEqAbs(sin(degreesToRadians(f32, 45)), sqrt(2.0)/2.0, 1e-6);

    // sin(pi/6) == sin(30°) == .5 
    try testing.expectApproxEqAbs(sin(degreesToRadians(f32, 30)), 0.5, 1e-6);
}



