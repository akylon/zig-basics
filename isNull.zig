const print = @import("std").debug.print;


//doesn't work ! ! !
//   ￬
//pub fn isUndefined(variable: anytype) bool {
//  if(variable == undefined) { 
//      return true;
//  }
//  else { return false; }
//}


pub fn isNull(variable: anytype) bool {
  if(variable==null){ return true; }
  else {return false;}
}



pub fn main() !void {
  //var w : u16 = null; // ￩ not allowed !!
  var u: ?u16 = undefined;
  var x: ?u16 = null;
  var y: ?u16 = 72;
  var z: ?u8 = 0;


  print("{}\n", .{isNull(u)});//false : undefined is not null
  print("{}\n", .{isNull(x)});//true
  print("{}\n", .{isNull(y)});//false
  print("{}\n", .{isNull(z)});//false
} 
