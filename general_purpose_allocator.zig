// GeneralPurposeAllocator demo



const std = @import("std");
const print = std.debug.print;


pub fn main() !void {

  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  const allocator = gpa.allocator();

  defer _ = gpa.deinit();

  var string : []u8 = try allocator.alloc(u8, 6);

  //std.mem.copy(u8, DEST, SOURCE);
  std.mem.copy(u8, string, "foobar");

  print("{s}\n", .{string});

  allocator.free(string);
}

