const std = @import("std");
const print = std.debug.print;

// references :
// https://en.wikipedia.org/wiki/Sorting_algorithm


// ~~~~ HELPER FUNCTIONS ~~~~


// swap lst[i] with lst[j
fn swap(lst: []u8, i: usize, j: usize) void {
   const tmp = lst[j];
   lst[j] = lst[i];
   lst[i] = tmp;
}


fn get_max(lst: []u8) u8 {
    var max: u8 = lst[0];

    var i: u8 = 1;
    while(i<lst.len):(i+=1){
        if(lst[i]>max) max = lst[i];
    }
    
    return max;
}


fn get_random_number(comptime T: type, min: T, max: T) !T {
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        try std.os.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    });

    const rand = prng.random();
    return rand.intRangeAtMost(T, min, max);
}


/// Fisher-Yates shuffle
fn shuffle(lst: []u8) !void {
    var i: usize = lst.len-1;
    while (i>0):(i-=1){
        const k = try get_random_number(usize, 0, i);
        const tmp = lst[i];
        lst[i] = lst[k];
        lst[k] = tmp;
    }
}

fn is_sorted(lst: []u8) bool {

    if(lst.len <= 1){
        return true;
    }
    
    var val: u8 = lst[0];
    
    for(lst[1..])|item|{
        if(item < val){
            return false;
        }
        val = item;
    }
    
    return true;
}

fn copy_array(lst: []u8, iBegin: usize, iEnd: usize, tmp: []u8) void {
    //seems to be called only once
    var i: usize = iBegin;
    while(i<iEnd):(i+=1){
        tmp[i] = lst[i]; 
    }
}




// ~~~~ BUBBLE SORT ~~~~
//FIXME : optimize bubble sort
// algorithm is from : https://en.wikipedia.org/wiki/Bubble_sort
// O(n^2)

pub fn bubbleSort(lst: []u8) void {
    while(true){
        var swapped: bool = false;
        var i: u8 = 1;

        while(i<lst.len):(i+=1){
            if(lst[i-1] > lst[i]){
                swap(lst, i-1, i);
                swapped = true;
            }
        }
        if(!swapped) break;
    }
}




// ~~~~ INSERTION SORT ~~~~
// O(n^2)
// https://en.wikipedia.org/wiki/Insertion_sort
pub fn insertion_sort(lst: []u8) void {
    for(lst, 0..)|_, i|{
        var j: usize = i;
        while(j>0 and lst[j-1] > lst[j]):(j-=1) {
            swap(lst, j, j-1); 
        } 
    }
}




// ~~~~ QUICKSORT ~~~~
// complexity O(n log n)
// divide & conquer, uses pivot, recursive algorithm
// algorithm is from : https://en.wikipedia.org/wiki/Quicksort#Algorithm
// lomuto partition scheme
fn partition(lst: []u8, low: i32, high: i32) i32 {
    const pivot = lst[@intCast(usize, high)]; // <-last element is pivot

    var tmp_pivot: i32 = low - 1;

    var j: i32 = low;
    while(j<high):(j+=1){
        if(lst[@intCast(u32, j)] <= pivot){
            tmp_pivot+=1;
            swap(lst, @intCast(usize, tmp_pivot), @intCast(usize, j));
        }
    }

    tmp_pivot+=1;
    swap(lst, @intCast(usize, tmp_pivot), @intCast(usize, high));
    return tmp_pivot;
}


pub fn quick_sort(lst: []u8, low: i32, high: i32) void {
    if(low>=high or low<0){
        return;
    }

    const pivot = partition(lst, low, high);

    quick_sort(lst, low, @intCast(i32, pivot-1)); //left of pivot
    quick_sort(lst, @intCast(i32, pivot+1), high); //right of pivot
}


// ~~~~ MERGE SORT ~~~~
// algorithm from : https://en.wikipedia.org/wiki/Merge_sort
// complexity : O(n log n)



pub fn top_down_merge_sort(A: []u8, B: []u8, n: usize) void {
    copy_array(A, 0, n, B);
    top_down_split_merge(B, 0, n, A);
}


pub fn top_down_split_merge(B: []u8, iBegin: usize, iEnd: usize, A: []u8) void {

    if(iEnd<iBegin or (iEnd-iBegin)<=1) return;

    const iMiddle = (iEnd+iBegin) / 2;

    top_down_split_merge(A, iBegin, iMiddle, B);
    top_down_split_merge(A, iMiddle, iEnd, B);

    top_down_merge(B, iBegin, iMiddle, iEnd, A);
}

pub fn top_down_merge(A: []u8, iBegin: usize, iMiddle: usize, iEnd: usize, B: []u8) void {
    var i: usize = iBegin;
    var j: usize = iMiddle;
    var k: usize = iBegin;

    while(k<iEnd):(k+=1){

        if(i<iMiddle and (j>=iEnd or A[i] <= A[j])){
            B[k] = A[i];
            i+=1;
        } else {
            B[k] = A[j]; 
            j+=1;
        }
    }
}


// ~~~~ HEAPSORT ~~~~
// algorithm from : https://en.wikipedia.org/wiki/Heapsort
// TODO


// ~~~~ TREESORT ~~~~
// algorithm from : https://en.wikipedia.org/wiki/Tree_sort
// TODO


// ~~~~ RADIXSORT ~~~~
// TODO


// ~~~~ BUCKETSORT ~~~~
pub fn bucket_sort(array: []u8, k: u8) void {
   _ = array;
  _ = k; 
}


// ~~~~ BOGOSORT ~~~~







pub fn bogosort(lst: []u8) !void {
    while(!is_sorted(lst[0..])){
        try shuffle(lst[0..]);
    }
}



pub fn main() !void {
    print("sorting algorithms\n", .{}); 

    var lst = [_]u8{45, 0, 12, 8, 24, 32, 90, 65, 67, 63};
    
    print("{}\n", .{is_sorted(lst[0..])});

    // bubbleSort(lst[0..]);
    // quick_sort(lst[0..], 0, lst.len-1);
    // insertion_sort(lst[0..]);
    
    // var tmp = [_]u8{0}**10;
    // top_down_merge_sort(lst[0..], tmp[0..], 10);
    print("{any}\n", .{lst});

    // print("{d}\n", .{get_max(lst[0..])});
    
    // print("{}\n", .{is_sorted(lst[0..])});
    
    try bogosort(lst[0..]);
    print("{any}\n", .{lst});
    print("{}\n", .{is_sorted(lst[0..])});
}
 
