// linear search and binary search implemented in zig
// run with zig test linear_and_binary_search.zig
// 
// algorithms are from video https://www.youtube.com/watch?v=8hly31xKli0&t=3651s
// from freeCodeCamp.org on youtube

const std = @import("std");
const testing = std.testing;


/// If our list was of size N = 6, this function would run 6 times. 
/// If our list was of size N = 72, this function would run 72 times.
/// Thus, time complexity is O(n)
/// also called linear time
fn linear_search(lst: []const u8, target: u8) ?usize {
    for (lst, 0..) |nb, i| {
        if (nb == target) {
            return i; 
        } 
    }
    return null;
}



/// time complexity : O(log n)
/// also called logarithmic time
fn binary_search(lst: []const u8, target: u8) ?usize {
    var first: usize = 0;
    var last: usize = lst.len-1;

    while (first<=last) {
        const midpoint  = @divTrunc((first+last), 2);

        if (lst[midpoint] == target) {
            return midpoint; 
        } else if (lst[midpoint] < target) {
            first = midpoint + 1; 
        } else {
            last = midpoint - 1; 
        }
    }
    return null;
}





fn recursive_binary_search(lst: []const u8, target: u8) bool {
    if (lst.len == 0) {
        return false;    
    } else {
        const midpoint = @divTrunc(lst.len, 2);

        if (lst[midpoint] == target) {
            return true;    
        } else {
            if (lst[midpoint] < target) {
                return recursive_binary_search(lst[midpoint+1..], target); 
            } 
            else {
                return recursive_binary_search(lst[0..midpoint], target); 
            }
        }
    }
}




test "linear search" {
    const lst = [_]u8{0,1,2,3,4,5,6,7,8,9,10};
    try testing.expect(linear_search(lst[0..], 11) == null);
    try testing.expect(linear_search(lst[0..], 9).? == 9);
    try testing.expect(linear_search(lst[0..], 3).? == 3);
}


test "binary search" {
    const lst = [_]u8{0,1,2,3,4,5,6,7,8,9,10};
    try testing.expect(binary_search(lst[0..], 11) == null);
    try testing.expect(binary_search(lst[0..], 9).? == 9);
    try testing.expect(binary_search(lst[0..], 2).? == 2);
}


test "recursive binary search" {
    const lst = [_]u8{0,1,2,3,4,5,6,7,8,9,10};
    try testing.expect(!recursive_binary_search(lst[0..], 11));
    try testing.expect(!recursive_binary_search(lst[0..], 120));
    try testing.expect(recursive_binary_search(lst[0..], 9));
}