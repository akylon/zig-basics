const std = @import("std");
const print = std.debug.print;


pub fn oneOrNull(x : u8) ?u8{
  if(x==1){ 
    return 1;
  }
  else {
    return null;
  }
}


pub fn main() void {
  print("{?}\n", .{oneOrNull(1)});  //1
  print("{?}\n", .{oneOrNull(0)});  //null
}
