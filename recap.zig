const std = @import("std");
const print = std.debug.print;
const testing = std.testing;


pub fn ano_struct() struct {x: u8, y: u8} {
    return .{
        .x = 0,
        .y = 255
    };
}





pub fn main() !void {



    print("\n~~~~ INITIALIZE ARRAY ~~~~\n", .{});
    // keywords : different ways to initialize arrays, how to initialize arrays, fill array, arrays not slices
    
    const array1 = [_]u8{'f', 'o', 'o'};

    const array2 = std.mem.zeroes([10]u8);

    var array3: [5]u8 = undefined;
    inline for(array3, 0..)|_, i|{
        array3[i] = 'A'; 
    }


    print("{any} -- {any} -- {any} \n", .{array1, array2, array3});
    print("~~~~    ~~~~\n\n\n", .{});














    print("\n~~~~ ARRAY TO SLICE ~~~~\n", .{});
    // keywords : slice and array, cohersion, const, mutable, immutable, declare array, array to slice, make slice from array

    const buffer2 = [_]u8{'f', 'o', 'o', 'b', 'a', 'r'};
    const slice: []const u8 = buffer2[0..];
    print("{s}\n", .{slice});

    // OR

    var buffer3 = [_]u8{'b', 'o', 'o', 'f', 'a', 'r'};
    const slice2: []u8 = buffer3[0..];
    var slice3: []u8 = buffer3[0..]; //mutable

    print("{s}\n", .{slice2});
    print("{s}\n", .{slice3});

    print("~~~~    ~~~~\n\n\n", .{});















    print("\n~~~~ ANONYMOUS STRUCT  ~~~~\n", .{});
    // keywords : anonymous struct, tuple, return anonymous tuple
    const ano = ano_struct();
    print("{d} {d} \n", .{ano.x, ano.y});
    print("~~~~    ~~~~\n\n\n", .{});












    print("\n~~~~ INT TO STRING ~~~~\n", .{});
    // keywords : convert int to string, format, int2string, int_to_string
    const number_as_int = 420;
    var buffer = std.mem.zeroes([10]u8); // we need a buffer to format the string
    const number_as_string = try std.fmt.bufPrint(buffer[0..], "{d}", .{number_as_int});
    print("{s}\n", .{number_as_string});
    print("~~~~    ~~~~\n\n\n", .{});


    







    print("\n~~~~  STRING TO INT ~~~~\n", .{});
    // keywords : convert string to int, string_to_int, string2int
    const int_as_string = "720";
    const int_as_int = try std.fmt.parseInt(u32, int_as_string, 10) + 5 ;
    print("{d}\n", .{int_as_int});
    print("~~~~    ~~~~\n\n\n", .{});















    print("\n~~~~ GPA ~~~~\n", .{});
    // keywords : gpa, general purpose allocator, detect memory leaks

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const gp_allocator = gpa.allocator();
    defer _ = gpa.deinit();
    
    var test_string = try gp_allocator.alloc(u8, 3);
    defer gp_allocator.free(test_string);
    std.mem.copy(u8, test_string, "foo");
    print("gpa test string : {s}\n", .{test_string});

    print("~~~~    ~~~~\n\n\n", .{});











    print("\n~~~~ ARENA ALLOCATOR ~~~~\n", .{});
    // keywords : arena, allocator, page, memory
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();// all arenas freed when arena.deinit() called
    const arena_allocator = arena.allocator();

    var test_string_2 = try arena_allocator.alloc(u8, 3);
    defer arena_allocator.free(test_string_2);
    std.mem.copy(u8, test_string_2, "bar");
    print("arena test string : {s}\n", .{test_string_2});

    print("~~~~    ~~~~\n\n\n", .{});













}

