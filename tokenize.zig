const std = @import("std");
const print = std.debug.print;


pub fn main() !void {
  const message = "this is a     test , ?.. lol";
  var it = std.mem.tokenize(u8, message, " ");
  while(it.next()) |item|{
    print("{s}\n", .{item});
  }
}
