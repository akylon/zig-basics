const std = @import("std");
const EXIT_CODE = 254;

pub fn main() void {
  std.os.exit(EXIT_CODE);
}
