// use with : `zig test array_of_slices.zig

const std = @import("std");
const print = std.debug.print;
const testing = std.testing;

test "array of slices" {
    const lst = [_][]const u8 {"foo", "bar", "foobar"};

    try testing.expect(std.mem.eql(u8, lst[0], "foo"));
    try testing.expect(std.mem.eql(u8, lst[1], "bar"));
    try testing.expect(std.mem.eql(u8, lst[2], "foobar"));
}
