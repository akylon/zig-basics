const std = @import("std");
const print = std.debug.print;


const Point = struct {
  x: u16 = 0,
  y: u16 = 0
};

var p1 = Point{};


//modifying glob var
pub fn modifyPoint() void {
  p1.x = 1;
  p1.y = 1;
}


//modifying with pointer
//do it this way
pub fn modifyPoint2(p: *Point) void {
  p.*.x = 2;
  p.*.y = 2;
}


//not allowed : cannot assign to constant
//pub fn modifyPoint3(p: Point) void {
//  p.x = 3;
//  p.y = 3;
//}


pub fn main() !void {
  modifyPoint();
  print("{d}\n", .{p1.x});//1
  print("{d}\n", .{p1.y});//1

  modifyPoint2(&p1);

  print("{d}\n", .{p1.x});//2
  print("{d}\n", .{p1.y});//2

  var p2 = Point{};

  print("{d}\n", .{p2.x}); //0
  print("{d}\n", .{p2.y}); //0

  modifyPoint2(&p2);

  print("{d}\n", .{p2.x}); //2
  print("{d}\n", .{p2.y}); //2
}
