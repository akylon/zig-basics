const std = @import("std");
const print = std.debug.print;

pub fn main() !void {
  var buf:[80]u8 = undefined;

  const tmp = std.fmt.bufPrint(buf[0..], "{d}", .{1756}) catch unreachable;
  print("{s}\n", .{tmp});


  const tmp2 = std.fmt.bufPrint(buf[0..], "{d}", .{24}) catch unreachable;
  print("{s}\n", .{tmp2});

}
