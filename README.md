# zig basics

## why ?

These are zig code snippets straight to the point.

For learning purpose.

## how to use :

```
zig build-exe file.zig
./file
```
or

```
zig test file.zig
```



## references :
https://ziglang.org/documentation/master/

https://ziglearn.org/

https://ziglang.org/documentation/master/std/#root

## zig version
latest